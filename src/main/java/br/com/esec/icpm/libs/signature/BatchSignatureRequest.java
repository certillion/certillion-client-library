package br.com.esec.icpm.libs.signature;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.esec.icpm.libs.signature.helper.MimeTypeConstants;
import br.com.esec.icpm.libs.signature.helper.RequestBatchSignatureHelper;
import br.com.esec.icpm.libs.signature.response.handler.SignatureAsynchHandler;
import br.com.esec.icpm.libs.signature.response.handler.SignatureBatchAsynchHandler;
import br.com.esec.icpm.libs.signature.response.handler.SignatureBatchSynchHandler;
import br.com.esec.icpm.libs.signature.response.handler.SignatureHandler;
import br.com.esec.icpm.mss.ws.BatchSignatureComplexDocumentRespType;
import br.com.esec.icpm.mss.ws.SignatureStandardType;
import br.com.esec.icpm.server.ws.ICPMException;

public class BatchSignatureRequest {

	private static Logger log = LoggerFactory.getLogger(SignatureRequest.class);

	private SignatureRequest request;

	private SignatureStandardType standard = SignatureStandardType.CADES;
	private String message;
	private List<Document> documents = new ArrayList<Document>();

	public BatchSignatureRequest(SignatureRequest request) {
		this.request = request;
	}

	public BatchSignatureRequest cades() {
		standard = SignatureStandardType.CADES;
		return this;
	}

	public BatchSignatureRequest adobePdf() {
		standard = SignatureStandardType.ADOBEPDF;
		return this;
	}

	public BatchSignatureRequest message(String message) {
		this.message = message;
		return this;
	}

	public BatchSignatureRequest document(String name, InputStream in) {
		this.documents.add(new Document(name, in));
		log.debug("Document '" + name + "' added.");
		return this;
	}

	public BatchSignatureRequest document(String name, InputStream in, String mymeType) {
		this.documents.add(new Document(name, in, mymeType));
		log.debug("Document '" + name + "' added.");
		return this;
	}

	public SignatureBatchSynchHandler sign() throws ICPMException, IOException {
		log.debug("Requesting asynch batch signature to '" + request.identifier + "' with standard '" + standard + "'.");
		BatchSignatureComplexDocumentRespType response = RequestBatchSignatureHelper.requestSynchBatchSignature(request.identifier, message, standard, request.testMode, documents);

		return new SignatureBatchSynchHandler(standard, response);
	}

	public SignatureBatchAsynchHandler asynchSign() throws ICPMException, IOException {
		log.debug("Requesting asynch batch signature to '" + request.identifier + "' with standard '" + standard + "'.");
		BatchSignatureComplexDocumentRespType response = RequestBatchSignatureHelper.requestAsynchBatchSignature(request.identifier, message, standard, request.testMode, documents);

		return new SignatureBatchAsynchHandler(standard, response);
	}

	public SignatureBatchAsynchHandler asynchSignWithNotify() throws ICPMException, IOException {
		log.debug("Requesting asynch batch signature to '" + request.identifier + "' with standard '" + standard + "'.");
		BatchSignatureComplexDocumentRespType response = RequestBatchSignatureHelper.requestAsynchWithNotifyBatchSignature(request.identifier, message, standard, request.testMode, documents);

		return new SignatureBatchAsynchHandler(standard, response);
	}

	public SignatureBatchAsynchHandler waitFor(long transactionId) throws ICPMException, InterruptedException {
		return new SignatureBatchAsynchHandler(standard, transactionId).waitTo();
	}

	public class Document {

		private final String name;
		private final InputStream in;
		private final String mimeType;

		public Document(String name, InputStream in) {
			this.name = name;
			this.in = in;
			final String extension = FilenameUtils.getExtension(name);
			if (StringUtils.isEmpty(extension))
				this.mimeType = "application/octet-stream";
			else
				this.mimeType = MimeTypeConstants.getMimeType(extension.toLowerCase());
		}

		public Document(String name, InputStream in, String mimeType) {
			this.name = name;
			this.in = in;
			this.mimeType = mimeType;
		}

		public String getName() {
			return name;
		}

		public InputStream getIn() {
			return in;
		}

		public String getMimeType() {
			return mimeType;
		}

	}
}
