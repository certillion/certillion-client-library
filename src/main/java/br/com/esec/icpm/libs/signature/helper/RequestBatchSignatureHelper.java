package br.com.esec.icpm.libs.signature.helper;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.esec.icpm.libs.signature.ApWsFactory;
import br.com.esec.icpm.libs.signature.BatchSignatureRequest.Document;
import br.com.esec.icpm.mss.rest.ApplicationProviderResource;
import br.com.esec.icpm.mss.ws.BatchSignatureComplexDocumentReqType;
import br.com.esec.icpm.mss.ws.BatchSignatureComplexDocumentRespType;
import br.com.esec.icpm.mss.ws.HashDocumentInfoType;
import br.com.esec.icpm.mss.ws.MessagingModeType;
import br.com.esec.icpm.mss.ws.SignaturePortType;
import br.com.esec.icpm.mss.ws.SignatureRespType;
import br.com.esec.icpm.mss.ws.SignatureSimpleDocumentReqType;
import br.com.esec.icpm.mss.ws.SignatureStandardType;
import br.com.esec.icpm.server.ws.ICPMException;
import br.com.esec.icpm.server.ws.MobileUserType;

public class RequestBatchSignatureHelper {

	private static Logger log = LoggerFactory.getLogger(RequestBatchSignatureHelper.class);

	public static BatchSignatureComplexDocumentRespType requestSynchBatchSignature(String identifier, String message, SignatureStandardType standard, boolean testMode,
			List<Document> documents) throws ICPMException {
		log.info("Requesting synch batch signature...");

		SignaturePortType signaturePort = ApWsFactory.getSoapService();
		BatchSignatureComplexDocumentReqType request = prepareBachRequest(identifier, message, standard, testMode, documents, MessagingModeType.SYNCH);
		BatchSignatureComplexDocumentRespType response = signaturePort.batchSignatureComplexDocument(request);

		log.info("Signature requested with transaction id " + response.getTransactionId() + ".");

		return response;
	}

	public static BatchSignatureComplexDocumentRespType requestAsynchBatchSignature(String identifier, String message, SignatureStandardType standard, boolean testMode,
			List<Document> documents) throws ICPMException {
		log.info("Requesting asynch batch signature...");

		SignaturePortType signaturePort = ApWsFactory.getSoapService();
		BatchSignatureComplexDocumentReqType request = prepareBachRequest(identifier, message, standard, testMode, documents, MessagingModeType.ASYNCH_CLIENT_SERVER);
		BatchSignatureComplexDocumentRespType response = signaturePort.batchSignatureComplexDocument(request);

		log.info("Signature requested with transaction id " + response.getTransactionId() + ".");

		return response;
	}

	public static BatchSignatureComplexDocumentRespType requestAsynchWithNotifyBatchSignature(String identifier, String message, SignatureStandardType standard, boolean testMode,
			List<Document> documents) throws ICPMException {
		log.info("Requesting asynch with notify batch signature...");

		SignaturePortType signaturePort = ApWsFactory.getSoapService();
		BatchSignatureComplexDocumentReqType request = prepareBachRequest(identifier, message, standard, testMode, documents, MessagingModeType.ASYNCH_SERVER_SERVER);
		BatchSignatureComplexDocumentRespType response = signaturePort.batchSignatureComplexDocument(request);

		log.info("Signature requested with transaction id " + response.getTransactionId() + ".");

		return response;
	}

	private static BatchSignatureComplexDocumentReqType prepareBachRequest(String identifier, String message, SignatureStandardType standard, boolean testMode,
			List<Document> documents, final MessagingModeType messagingMode) {
		// Set the mobileUser
		MobileUserType mobileUser = new MobileUserType();
		mobileUser.setUniqueIdentifier(identifier);

		// Create the batch request
		BatchSignatureComplexDocumentReqType request = new BatchSignatureComplexDocumentReqType();
		request.setMobileUser(mobileUser);
		request.setMessagingMode(messagingMode);
		request.setDataToBeDisplayed(message);
		request.setSignatureStandard(standard);
		request.setTestMode(testMode);

		// Set the request document
		List<HashDocumentInfoType> documentInfos = handleDocuments(documents);
		request.setDocumentsToBeSigned(documentInfos);
		return request;
	}

	private static List<HashDocumentInfoType> handleDocuments(List<Document> documents) {
		// upload files from each url
		List<HashDocumentInfoType> documentInfos = new ArrayList<HashDocumentInfoType>();
		for (Document document : documents) {
			log.info("Uploading document " + document.getName());

			final String hash = upload(document.getIn());
			documentInfos.add(getDocumentInfo(hash, document.getName(), document.getMimeType()));
		}
		return documentInfos;
	}

	private static HashDocumentInfoType getDocumentInfo(final String hash, final String name, final String mimeType) {
		// Get hash from uploaded file, create document and save the document in the list
		HashDocumentInfoType document = new HashDocumentInfoType();
		document.setDocumentName(name);
		document.setHash(hash);
		document.setContentType(mimeType);

		return document;
	}

	private static String upload(InputStream in) {
		try {
			ApplicationProviderResource restService = ApWsFactory.getRestService();
			return restService.upload(in);
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

}
