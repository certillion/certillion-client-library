package br.com.esec.icpm.libs.signature.response.handler;

import br.com.esec.icpm.server.ws.ICPMException;

public interface SignatureAsynchHandler extends SignatureHandler {

	long DEFAULT_TIMEOUT = 30; // 30s
	
	SignatureAsynchHandler waitTo() throws ICPMException, InterruptedException;

	SignatureAsynchHandler waitTo(long timeout) throws ICPMException, InterruptedException;

}