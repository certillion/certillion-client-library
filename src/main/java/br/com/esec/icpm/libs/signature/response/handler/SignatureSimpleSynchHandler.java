package br.com.esec.icpm.libs.signature.response.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

import br.com.esec.icpm.mss.ws.SignatureRespType;
import br.com.esec.icpm.server.factory.Status;

public class SignatureSimpleSynchHandler extends BaseSignatureHandler {

	private InputStream signature;

	public SignatureSimpleSynchHandler(long transactionId) {
		super(transactionId);
	}

	public SignatureSimpleSynchHandler(SignatureRespType response) {
		super(response.getTransactionId());

		if (response.getStatus() != null) {
			int statusCode = response.getStatus().getStatusCode();
			String statusMessage = response.getStatus().getStatusMessage();
			if (statusCode != Status.SIGNATURE_VALID.getCode()) {
				throw new IllegalStateException("Something bad happened. The signature response return eith status " + statusMessage + " (" + statusCode + ")."); // TODO
			}
		}

		try {
			if (response.getSignature() != null) {
				signature = new ByteArrayInputStream(IOUtils.toByteArray(response.getSignature().getInputStream()));
			}
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public SignatureHandler save(OutputStream out) throws IOException {
		IOUtils.copy(signature, out);
		return this;
	}

	@Override
	public SignatureHandler save(InputStream in, OutputStream out) throws IOException {
		// TODO: Implementar attach de assinatura (CMS)
		throw new IllegalStateException("Implement it!!!");
	}

}
