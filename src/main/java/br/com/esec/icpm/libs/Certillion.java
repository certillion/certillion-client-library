package br.com.esec.icpm.libs;

import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import br.com.esec.icpm.libs.signature.SignatureRequest;

public class Certillion {

	static {
		if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
			Security.addProvider(new BouncyCastleProvider());
		}
	}
	
	public static Configurable config() {
		return new Configurable();
	}
	
	public static SignatureRequest signature() {
		return new SignatureRequest();
	}
	
}
