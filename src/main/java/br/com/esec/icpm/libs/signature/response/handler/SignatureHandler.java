package br.com.esec.icpm.libs.signature.response.handler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface SignatureHandler {

	public abstract SignatureHandler save(OutputStream out) throws IOException;

	public abstract SignatureHandler save(InputStream in, OutputStream out) throws IOException;

}