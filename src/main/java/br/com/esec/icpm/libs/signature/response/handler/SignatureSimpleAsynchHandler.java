package br.com.esec.icpm.libs.signature.response.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

import br.com.esec.icpm.libs.signature.helper.RequestStatusHelper;
import br.com.esec.icpm.mss.ws.SignatureRespType;
import br.com.esec.icpm.mss.ws.SignatureStatusRespType;
import br.com.esec.icpm.server.factory.Status;
import br.com.esec.icpm.server.ws.ICPMException;

public class SignatureSimpleAsynchHandler extends BaseSignatureHandler implements SignatureAsynchHandler {

	private InputStream signature;

	public SignatureSimpleAsynchHandler(long transactionId) {
		super(transactionId);
	}

	public SignatureSimpleAsynchHandler(SignatureRespType response) {
		super(response.getTransactionId());

		if (response.getStatus() != null) {
			int statusCode = response.getStatus().getStatusCode();
			String statusMessage = response.getStatus().getStatusMessage();
			if (statusCode != Status.SIGNATURE_VALID.getCode()) {
				throw new IllegalStateException("Something bad happened. The signature response return with status " + statusMessage + " (" + statusCode + ")."); // TODO
			}
		}

		try {
			if (response.getSignature() != null) {
				signature = new ByteArrayInputStream(IOUtils.toByteArray(response.getSignature().getInputStream()));
			}
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public SignatureSimpleAsynchHandler waitTo() throws ICPMException, InterruptedException {
		return waitTo(DEFAULT_TIMEOUT);
	}

	@Override
	public SignatureSimpleAsynchHandler waitTo(long timeout) throws ICPMException, InterruptedException {
		SignatureStatusRespType signatureStatusResp = RequestStatusHelper.requestStatus(transactionId);
		while (signatureStatusResp.getStatus().getStatusCode() == Status.TRANSACTION_IN_PROGRESS.getCode()) {
			Thread.sleep(2 * 1000); // Stop for 2 seconds
			// TODO: Implement TIMEOUT

			signatureStatusResp = RequestStatusHelper.requestStatus(transactionId);
		}
		
		if (signatureStatusResp.getStatus() != null) {
			int statusCode = signatureStatusResp.getStatus().getStatusCode();
			String statusMessage = signatureStatusResp.getStatus().getStatusMessage();
			if (statusCode != Status.SIGNATURE_VALID.getCode()) {
				throw new IllegalStateException("Something bad happened. The signature response return with status " + statusMessage + " (" + statusCode + ")."); // TODO
			
			}
		}
		try {
			if (signatureStatusResp.getSignature() != null) {
				signature = new ByteArrayInputStream(IOUtils.toByteArray(signatureStatusResp.getSignature().getInputStream()));
			}
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}

		return this;
	}

	@Override
	public SignatureHandler save(OutputStream out) throws IOException {
		IOUtils.copy(signature, out);
		return this;
	}

	@Override
	public SignatureHandler save(InputStream in, OutputStream out) throws IOException {
		// TODO: Implementar attach de assinatura (CMS)
		throw new IllegalStateException("Implement it!!!");
	}

}
