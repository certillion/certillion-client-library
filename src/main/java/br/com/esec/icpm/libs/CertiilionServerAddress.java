package br.com.esec.icpm.libs;

import java.net.URL;

public class CertiilionServerAddress {

	private static final String ADDRESS = "https://api.certillion.com";

	public static String get() {
		check();
		return ADDRESS;
	}

	protected static void check() {
		final String testUrl = ADDRESS + "/mss-ws/SignatureService/SignatureEndpointBean?wsdl";
		try {
			new URL(testUrl).openStream();
		} catch (Exception e) {
			System.err.println("Could not connect to '" + testUrl + "'. Check your WS-Signer configuration.");
			System.exit(1);
		}
	}
}
