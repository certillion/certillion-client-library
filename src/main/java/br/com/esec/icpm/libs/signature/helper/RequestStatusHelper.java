package br.com.esec.icpm.libs.signature.helper;

import br.com.esec.icpm.libs.signature.ApWsFactory;
import br.com.esec.icpm.mss.ws.BatchSignatureTIDsRespType;
import br.com.esec.icpm.mss.ws.SignaturePortType;
import br.com.esec.icpm.mss.ws.SignatureStatusReqType;
import br.com.esec.icpm.mss.ws.SignatureStatusRespType;
import br.com.esec.icpm.server.ws.ICPMException;

public class RequestStatusHelper {

	public static BatchSignatureTIDsRespType requestBatchStatus(long transactionId) throws ICPMException {
		SignaturePortType signaturePort = ApWsFactory.getSoapService();
		
		SignatureStatusReqType signatureStatusReq = new SignatureStatusReqType();
		signatureStatusReq.setTransactionId(transactionId);

		return signaturePort.batchSignatureTIDsStatus(signatureStatusReq);
	}

	public static SignatureStatusRespType requestStatus(long transactionId) throws ICPMException {
		SignaturePortType signatureService = ApWsFactory.getSoapService();
		
		SignatureStatusReqType signatureStatusReq = new SignatureStatusReqType();
		signatureStatusReq.setTransactionId(transactionId);
		
		return signatureService.statusQuery(signatureStatusReq);
		
	}

}
