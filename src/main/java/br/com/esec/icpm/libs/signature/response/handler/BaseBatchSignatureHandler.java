package br.com.esec.icpm.libs.signature.response.handler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Deque;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.esec.icpm.mss.ws.SignatureStandardType;
import br.com.esec.icpm.server.factory.Status;

public abstract class BaseBatchSignatureHandler extends BaseSignatureHandler {
	
	private static Logger log = LoggerFactory.getLogger(BaseBatchSignatureHandler.class);
	
	protected SignatureStandardType standard;
	
	protected Deque<DocumentStatus> documentsStatus = new LinkedList<SignatureBatchSynchHandler.DocumentStatus>();

	public BaseBatchSignatureHandler(SignatureStandardType standard, long transactionId) {
		super(transactionId);
		this.standard = standard;
	}

	@Override
	public SignatureHandler save(OutputStream out) throws IOException {

		DocumentStatus docStatus = documentsStatus.removeFirst();

		log.info("Saving document signature of document '" + docStatus.getName() + "'.");

		if (docStatus.getStatusCode() == Status.SIGNATURE_VALID.getCode()) {
			switch (standard) {
			case CADES:
				saveCades(out, docStatus.getSignature());
				break;
			case ADOBEPDF:
				throw new IllegalStateException("Error. To save with AdobePdf we need the stream to original document again.");
			}
		} else {
			throw new IllegalStateException("Implement it!!!"); // TODO
		}

		return this;
	}

	@Override
	public SignatureHandler save(InputStream in, OutputStream out) throws IOException {

		DocumentStatus docStatus = documentsStatus.removeFirst();

		log.info("Saving document signature of document '" + docStatus.getName() + "'.");

		if (docStatus.getStatusCode() == Status.SIGNATURE_VALID.getCode()) {
			switch (standard) {
			case CADES:
				saveCades(out, docStatus.getSignature());
				break;
			case ADOBEPDF:
				saveAdobePdf(in, out, docStatus.getSignature());
			}
		} else {
			throw new IllegalStateException("Implement it!!!"); // TODO
		}

		return this;
	}

	public class DocumentStatus {

		private final long docId;
		private final String name;
		private final int statusCode;
		private final InputStream signature;

		public DocumentStatus(long docId, String name, int statusCode, InputStream signature) {
			this.docId = docId;
			this.name = name;
			this.statusCode = statusCode;
			this.signature = signature;
		}

		public String getName() {
			return name;
		}

		public int getStatusCode() {
			return statusCode;
		}

		public InputStream getSignature() {
			return signature;
		}

		public long getDocId() {
			return docId;
		}

	}
}
