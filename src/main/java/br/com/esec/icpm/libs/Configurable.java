package br.com.esec.icpm.libs;

import br.com.esec.icpm.signer.security.SecurityConfig;

public class Configurable {

	private String keystorePath;
	private String keystoreType;
	private String keystorePassword;
	private String keyAlias;
	private String truststorePath;
	private String truststoreType;
	private String truststorePassword;
	private boolean allowAllHosts;

	public void done() {
		SecurityConfig.set(keystorePath, keystoreType, keystorePassword, keyAlias, truststorePath, truststoreType, truststorePassword, allowAllHosts);
		SecurityConfig.validate();
	}

	public Configurable keystorePath(String keystorePath) {
		this.keystorePath = keystorePath;
		return this;
	}

	public Configurable keystoreType(String keystoreType) {
		this.keystoreType = keystoreType;
		return this;
	}

	public Configurable keystorePassword(String keystorePassword) {
		this.keystorePassword = keystorePassword;
		return this;
	}

	public Configurable keyAlias(String keyAlias) {
		this.keyAlias = keyAlias;
		return this;
	}

	public Configurable truststorePath(String truststorePath) {
		this.truststorePath = truststorePath;
		return this;
	}

	public Configurable truststoreType(String truststoreType) {
		this.truststoreType = truststoreType;
		return this;
	}

	public Configurable truststorePassword(String truststorePassword) {
		this.truststorePassword = truststorePassword;
		return this;
	}

	public Configurable allowAllHosts(boolean allowAllHosts) {
		this.allowAllHosts = allowAllHosts;
		return this;
	}

}
