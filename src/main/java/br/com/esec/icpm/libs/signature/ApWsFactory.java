package br.com.esec.icpm.libs.signature;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.xml.ws.Service;

import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import br.com.esec.icpm.libs.CertiilionServerAddress;
import br.com.esec.icpm.mss.rest.ApplicationProviderResource;
import br.com.esec.icpm.mss.ws.SignaturePortType;
import br.com.esec.icpm.signer.ws.rest.SslUtils;
import br.com.esec.icpm.signer.ws.soap.CxfUtils;

public class ApWsFactory {

	private static final String AP_REST_WS_URI = CertiilionServerAddress.get() + "/mss/restful/";
	private static final String SIGNATURE_SOAP_WS_URL = CertiilionServerAddress.get() + "/mss-ws/SignatureService/SignatureEndpointBean?wsdl";

	private static ClientBuilder builder;

	public static SignaturePortType getSoapService() {
		try {
			URL signatureRequestSoapWsUrl = new URL(SIGNATURE_SOAP_WS_URL);
			Service signatureService = Service.create(signatureRequestSoapWsUrl, SignaturePortType.QNAME);
			final SignaturePortType port = signatureService.getPort(SignaturePortType.class);

			CxfUtils.config(port);

			return port;
		} catch (MalformedURLException e) {
			throw new IllegalStateException(e);
		}
	}

	public static ApplicationProviderResource getRestService() {
		try {
			if (builder == null) {
				generateBuilder();
			}
			
			// HTTP Client
			Client client = builder.build();

			// Proxy
			WebTarget target = client.target(AP_REST_WS_URI);
			ResteasyWebTarget rtarget = (ResteasyWebTarget) target;
			ApplicationProviderResource restProxy = rtarget.proxy(ApplicationProviderResource.class);

			return restProxy;
		} catch (KeyManagementException e) {
			throw new IllegalStateException(e);
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e);
		} catch (UnrecoverableKeyException e) {
			throw new IllegalStateException(e);
		} catch (KeyStoreException e) {
			throw new IllegalStateException(e);
		} catch (CertificateException e) {
			throw new IllegalStateException(e);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	private static synchronized void generateBuilder() throws NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, IOException, CertificateException,
			KeyManagementException {
		builder = ClientBuilder.newBuilder();
		SslUtils.config(builder);
	}

}
