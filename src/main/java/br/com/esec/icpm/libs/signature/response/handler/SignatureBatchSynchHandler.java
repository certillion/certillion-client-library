package br.com.esec.icpm.libs.signature.response.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.esec.icpm.libs.signature.helper.RequestStatusHelper;
import br.com.esec.icpm.mss.ws.BatchSignatureComplexDocumentRespType;
import br.com.esec.icpm.mss.ws.DocumentSignatureInfoType;
import br.com.esec.icpm.mss.ws.SignatureStandardType;
import br.com.esec.icpm.mss.ws.SignatureStatusRespType;
import br.com.esec.icpm.server.factory.Status;

public class SignatureBatchSynchHandler extends BaseBatchSignatureHandler {

	private static Logger log = LoggerFactory.getLogger(SignatureBatchSynchHandler.class);

	private Deque<DocumentStatus> docStatuses = new LinkedList<SignatureBatchSynchHandler.DocumentStatus>();

	public SignatureBatchSynchHandler(SignatureStandardType standard, BatchSignatureComplexDocumentRespType response) throws IOException {
		super(standard, response.getTransactionId());

		if (response.getStatus() != null) {
			int statusCode = response.getStatus().getStatusCode();
			String statusMessage = response.getStatus().getStatusMessage();
			if (statusCode != Status.REQUEST_OK.getCode()) {
				throw new IllegalStateException("Something bad happened. The signature response return eith status " + statusMessage + " (" + statusCode + ")."); // TODO
			}
		}
		
		List<DocumentSignatureInfoType> signatures = response.getSignatures();
		if (signatures != null) {
			for (DocumentSignatureInfoType docInfo : signatures) {
				final int statusCode = docInfo.getStatus().getStatusCode();
				final String name = docInfo.getDocumentName();
				
				InputStream signature = null;
				if (statusCode == Status.SIGNATURE_VALID.getCode()) {
					try {
						if (docInfo.getSignature() != null) {
							signature = new ByteArrayInputStream(IOUtils.toByteArray(docInfo.getSignature().getInputStream()));
						}
					} catch (IOException e) {
						throw new IllegalStateException(e);
					}
				}
				
				docStatuses.add(new DocumentStatus(-1l, name, statusCode, signature));
			}
		}
	}

}
