package br.com.esec.icpm.libs.signature;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.esec.icpm.libs.signature.helper.RequestSimpleSignatureHelper;
import br.com.esec.icpm.libs.signature.response.handler.SignatureSimpleAsynchHandler;
import br.com.esec.icpm.libs.signature.response.handler.SignatureSimpleSynchHandler;
import br.com.esec.icpm.mss.ws.SignatureRespType;
import br.com.esec.icpm.server.ws.ICPMException;

public class SimpleSignatureRequest {

	private static Logger log = LoggerFactory.getLogger(SignatureRequest.class);

	private SignatureRequest request;

	private String message;

	public SimpleSignatureRequest(SignatureRequest request) {
		this.request = request;
	}

	public SimpleSignatureRequest message(String message) {
		this.message = message;
		return this;
	}

	public SignatureSimpleSynchHandler sign() throws ICPMException, IOException {
		log.debug("Requesting asynch batch signature to '" + request.identifier + "'.");
		SignatureRespType response = RequestSimpleSignatureHelper.requestSynchSimpleSignature(request.identifier, message, request.testMode);

		return new SignatureSimpleSynchHandler(response);
	}

	public SignatureSimpleAsynchHandler asynchSign() throws ICPMException, IOException {
		log.debug("Requesting asynch batch signature to '" + request.identifier + "'.");
		SignatureRespType response = RequestSimpleSignatureHelper.requestAsynchSimpleSignature(request.identifier, message, request.testMode);

		return new SignatureSimpleAsynchHandler(response);
	}

	public SignatureSimpleAsynchHandler asynchSignWithNotify() throws ICPMException, IOException {
		log.debug("Requesting asynch batch signature to '" + request.identifier + "'.");
		SignatureRespType response = RequestSimpleSignatureHelper.requestAsynchWithNotifySimpleSignature(request.identifier, message, request.testMode);

		return new SignatureSimpleAsynchHandler(response);
	}

	public SignatureSimpleAsynchHandler waitFor(long transactionId) throws ICPMException, InterruptedException {
		return new SignatureSimpleAsynchHandler(transactionId).waitTo();
	}
}
