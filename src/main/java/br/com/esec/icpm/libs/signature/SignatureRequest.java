package br.com.esec.icpm.libs.signature;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SignatureRequest {

	private static Logger log = LoggerFactory.getLogger(SignatureRequest.class);

	String identifier;
	String message;

	boolean testMode;

	public SignatureRequest testMode() {
		testMode = true;
		return this;
	}

	public SignatureRequest toUser(String identifier) {
		this.identifier = identifier;
		return this;
	}

	public SignatureRequest message(String message) {
		this.message = message;
		return this;
	}
	
	public SimpleSignatureRequest simple() {
		return new SimpleSignatureRequest(this);
	}
	
	public BatchSignatureRequest batch() {
		return new BatchSignatureRequest(this);
	}
}
