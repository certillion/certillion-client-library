package br.com.esec.icpm.libs.signature.helper;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.esec.icpm.libs.signature.ApWsFactory;
import br.com.esec.icpm.libs.signature.BatchSignatureRequest.Document;
import br.com.esec.icpm.mss.rest.ApplicationProviderResource;
import br.com.esec.icpm.mss.ws.BatchSignatureComplexDocumentReqType;
import br.com.esec.icpm.mss.ws.BatchSignatureComplexDocumentRespType;
import br.com.esec.icpm.mss.ws.HashDocumentInfoType;
import br.com.esec.icpm.mss.ws.MessagingModeType;
import br.com.esec.icpm.mss.ws.SignaturePortType;
import br.com.esec.icpm.mss.ws.SignatureRespType;
import br.com.esec.icpm.mss.ws.SignatureSimpleDocumentReqType;
import br.com.esec.icpm.mss.ws.SignatureStandardType;
import br.com.esec.icpm.server.ws.ICPMException;
import br.com.esec.icpm.server.ws.MobileUserType;

public class RequestSimpleSignatureHelper {

	private static Logger log = LoggerFactory.getLogger(RequestSimpleSignatureHelper.class);

	public static SignatureRespType requestSynchSimpleSignature(String identifier, String message, boolean testMode) throws ICPMException {
		log.info("Requesting synch simple signature...");

		SignaturePortType signaturePort = ApWsFactory.getSoapService();
		SignatureSimpleDocumentReqType request = prepareSimpleRequest(identifier, message, testMode, MessagingModeType.SYNCH);
		SignatureRespType response = signaturePort.signatureSimpleDocument(request);

		log.info("Signature requested with transaction id " + response.getTransactionId() + ".");

		return response;
	}

	public static SignatureRespType requestAsynchSimpleSignature(String identifier, String message, boolean testMode) throws ICPMException {
		log.info("Requesting asynch simple signature...");

		SignaturePortType signaturePort = ApWsFactory.getSoapService();
		SignatureSimpleDocumentReqType request = prepareSimpleRequest(identifier, message, testMode, MessagingModeType.ASYNCH_CLIENT_SERVER);
		SignatureRespType response = signaturePort.signatureSimpleDocument(request);

		log.info("Signature requested with transaction id " + response.getTransactionId() + ".");

		return response;
	}

	public static SignatureRespType requestAsynchWithNotifySimpleSignature(String identifier, String message, boolean testMode) throws ICPMException {
		log.info("Requesting asynch simple signature...");

		SignaturePortType signaturePort = ApWsFactory.getSoapService();
		SignatureSimpleDocumentReqType request = prepareSimpleRequest(identifier, message, testMode, MessagingModeType.ASYNCH_SERVER_SERVER);
		SignatureRespType response = signaturePort.signatureSimpleDocument(request);

		log.info("Signature requested with transaction id " + response.getTransactionId() + ".");

		return response;
	}

	private static SignatureSimpleDocumentReqType prepareSimpleRequest(String identifier, String message, boolean testMode, final MessagingModeType messagingMode) {
		// Set the mobileUser
		MobileUserType mobileUser = new MobileUserType();
		mobileUser.setUniqueIdentifier(identifier);

		// Set the signature request
		SignatureSimpleDocumentReqType request = new SignatureSimpleDocumentReqType();
		request.setDataToBeSigned(message);
		request.setMobileUser(mobileUser);
		request.setMessagingMode(messagingMode);
		request.setTestMode(testMode);

		return request;
	}

}
