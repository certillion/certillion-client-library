package br.com.esec.icpm.libs.signature.response.handler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

import br.com.esec.icpm.libs.signature.helper.pdf.AttachSignatureHelper;

public abstract class BaseSignatureHandler implements SignatureHandler {

	protected long transactionId;

	public BaseSignatureHandler(long transactionId) {
		this.transactionId = transactionId;
	}

	protected void saveCades(OutputStream out, InputStream signature) throws IOException {
		IOUtils.copy(signature, out);
	}

	protected void saveAdobePdf(InputStream originalPdf, OutputStream signedPdf, InputStream signature) {
		AttachSignatureHelper.attach(transactionId, originalPdf, signature, signedPdf);
	}

}