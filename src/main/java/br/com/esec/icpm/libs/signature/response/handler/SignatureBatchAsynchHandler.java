package br.com.esec.icpm.libs.signature.response.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.esec.icpm.libs.signature.helper.RequestStatusHelper;
import br.com.esec.icpm.mss.ws.BatchSignatureComplexDocumentRespType;
import br.com.esec.icpm.mss.ws.BatchSignatureTIDsRespType;
import br.com.esec.icpm.mss.ws.DocumentSignatureStatusInfoType;
import br.com.esec.icpm.mss.ws.SignatureStandardType;
import br.com.esec.icpm.mss.ws.SignatureStatusRespType;
import br.com.esec.icpm.server.factory.Status;
import br.com.esec.icpm.server.ws.ICPMException;

public class SignatureBatchAsynchHandler extends BaseBatchSignatureHandler implements SignatureAsynchHandler {

	private static Logger log = LoggerFactory.getLogger(SignatureBatchAsynchHandler.class);

	public SignatureBatchAsynchHandler(SignatureStandardType standard, BatchSignatureComplexDocumentRespType response) throws IOException {
		super(standard, response.getTransactionId());

		if (response.getStatus() != null) {
			int statusCode = response.getStatus().getStatusCode();
			String statusMessage = response.getStatus().getStatusMessage();
			if (statusCode != Status.REQUEST_OK.getCode()) {
				throw new IllegalStateException("Something bad happened. The signature response return eith status " + statusMessage + " (" + statusCode + ")."); // TODO
			}
		}
	}

	public SignatureBatchAsynchHandler(SignatureStandardType standard, long transactionId) {
		super(standard, transactionId);
	}

	@Override
	public SignatureBatchAsynchHandler waitTo() throws ICPMException, InterruptedException {
		return waitTo(DEFAULT_TIMEOUT);
	}

	@Override
	public SignatureBatchAsynchHandler waitTo(long timeout) throws ICPMException, InterruptedException {

		BatchSignatureTIDsRespType signatureStatusResp = RequestStatusHelper.requestBatchStatus(transactionId);
		while (signatureStatusResp.getStatus().getStatusCode() == Status.TRANSACTION_IN_PROGRESS.getCode()) {
			Thread.sleep(2 * 1000); // Stop for 2 seconds
			// TODO: Implement TIMEOUT

			signatureStatusResp = RequestStatusHelper.requestBatchStatus(transactionId);
		}

		documentsStatus = parseDocumentStatus(signatureStatusResp.getDocumentSignatureStatus());

		return this;
	}

	public SignatureInfos waitForDocInfos() throws ICPMException, InterruptedException {
		return waitForDocInfos(DEFAULT_TIMEOUT);
	}

	public SignatureInfos waitForDocInfos(long timeout) throws ICPMException, InterruptedException {
		BatchSignatureTIDsRespType signatureStatusResp = RequestStatusHelper.requestBatchStatus(transactionId);
		while (signatureStatusResp.getDocumentSignatureStatus() == null) {
			Thread.sleep(2 * 1000); // Stop for 2 seconds
			// TODO: Implement TIMEOUT

			signatureStatusResp = RequestStatusHelper.requestBatchStatus(transactionId);
		}

		documentsStatus = parseDocumentStatus(signatureStatusResp.getDocumentSignatureStatus());
		
		return new SignatureInfos(transactionId, documentsStatus);
	}

	private Deque<DocumentStatus> parseDocumentStatus(List<DocumentSignatureStatusInfoType> signatures) throws ICPMException {
		Deque<DocumentStatus> documentsStatus = new LinkedList<SignatureBatchSynchHandler.DocumentStatus>();
		for (DocumentSignatureStatusInfoType docStatusInfo : signatures) {
			final long docId = docStatusInfo.getTransactionId();
			final int statusCode = docStatusInfo.getStatus().getStatusCode();
			final String name = docStatusInfo.getDocumentName();

			InputStream signature = null;
			if (statusCode == Status.SIGNATURE_VALID.getCode()) {
				SignatureStatusRespType status = RequestStatusHelper.requestStatus(docId);
				try {
					if (status.getSignature() != null) {
						signature = new ByteArrayInputStream(IOUtils.toByteArray(status.getSignature().getInputStream()));
					}
				} catch (IOException e) {
					throw new IllegalStateException(e);
				}
			}

			documentsStatus.add(new DocumentStatus(docId, name, statusCode, signature));
		}
		return documentsStatus;
	}

	public class SignatureInfos {

		private final long transactionId;
		private final Deque<DocumentStatus> documentsStatus;

		public SignatureInfos(long transactionId, Deque<DocumentStatus> documentsStatus) {
			this.transactionId = transactionId;
			this.documentsStatus = documentsStatus;
		}

		public long getTransactionId() {
			return transactionId;
		}

		public Deque<DocumentStatus> getDocumentsStatus() {
			return documentsStatus;
		}

	}

}
