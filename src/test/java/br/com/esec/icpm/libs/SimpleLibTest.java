package br.com.esec.icpm.libs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Deque;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.esec.icpm.libs.signature.response.handler.SignatureBatchAsynchHandler;
import br.com.esec.icpm.libs.signature.response.handler.SignatureBatchAsynchHandler.SignatureInfos;
import br.com.esec.icpm.libs.signature.response.handler.BaseBatchSignatureHandler.DocumentStatus;

public class SimpleLibTest {
	
	private static Logger log = LoggerFactory.getLogger(SignatureBatchAsynchHandler.class);

	@BeforeClass
	public static void init() {
		// config
		Certillion
			.config()
			.keystorePath("C:/Users/Tales Porto/Documents/Ferramentas/ws-signer-prod/certillion_final.jks")
			.keystoreType("JKS")
			.keyAlias("*.certillion.com")
			.keystorePassword("")
			.done();
	}
	
	
	@Test
	public void testSynchCadesSimpleSignature() throws Exception {
		// Synch
		Certillion
			.signature()
			.toUser("talesap@gmail.com")
			.simple()
			.message("Petição da Lava Jato")
			.sign()
			.save(getOutputStream1());
	}
	
	@Test
	public void testAsynchCadesSimpleSignature() throws Exception {
		// Asynch Client-Server (Polling)
		Certillion
			.signature()
			.toUser("talesap@gmail.com")
			.simple()
			.message("Petição da Lava Jato")
			.asynchSign()
			.waitTo(30)
			.save(getOutputStream2());
	}
	
	private FileOutputStream getOutputStream2() throws FileNotFoundException {
		return new FileOutputStream("C:\\Users\\Tales Porto\\Downloads\\POLÍTICA INTERNA PDF [SIGNED].pdf");
	}


	private FileOutputStream getOutputStream1() throws FileNotFoundException {
		return new FileOutputStream("C:\\Users\\Tales Porto\\Downloads\\contrato [SIGNED].pdf");
	}


	private FileInputStream getInputStream2() throws FileNotFoundException {
		return new FileInputStream("C:\\Users\\Tales Porto\\Downloads\\POLÍTICA INTERNA PDF.pdf");
	}


	private FileInputStream getInputStream1() throws FileNotFoundException {
		return new FileInputStream("C:\\Users\\Tales Porto\\Downloads\\contrato.pdf");
	}
}
