package br.com.esec.icpm.libs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Deque;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.esec.icpm.libs.signature.response.handler.SignatureBatchAsynchHandler;
import br.com.esec.icpm.libs.signature.response.handler.SignatureBatchAsynchHandler.SignatureInfos;
import br.com.esec.icpm.libs.signature.response.handler.BaseBatchSignatureHandler.DocumentStatus;

public class BatchLibTest {
	
	private static Logger log = LoggerFactory.getLogger(SignatureBatchAsynchHandler.class);

	@BeforeClass
	public static void init() {
		// Config
		Certillion
			.config()
			.keystorePath("C:/Users/Tales Porto/Documents/Ferramentas/ws-signer-prod/certillion_final.jks")
			.keystoreType("JKS")
			.keyAlias("*.certillion.com")
			.keystorePassword("")
			.done();
	}
	
	@Test
	public void testSynchCadesBatchSignature() throws Exception {
		// Synch
		Certillion
			.signature()
			.toUser("talesap@gmail.com")
			.batch()
			.cades()
			.message("Petição da Lava Jato")
			.document("relátorio ministro 1", getInputStream1())
			.document("relátorio ministro 2", getInputStream2())
			.sign()
			.save(getOutputStream1())
			.save(getOutputStream2());
	}
	
	@Test
	public void testSynchAdobePdfBatchSignature() throws Exception {
		// Synch
		Certillion
			.signature()
			.toUser("talesap@gmail.com")
			.batch()
			.adobePdf()
			.message("Petição da Lava Jato")
			.document("relátorio ministro 1", getInputStream1())
			.document("relátorio ministro 2", getInputStream2())
			.sign()
			.save(getInputStream1(), getOutputStream1())
			.save(getInputStream2(), getOutputStream2());
	}

	@Test
	public void testAsynchCadesBatchSignature() throws Exception {
		// Asynch Client-Server (Polling)
		Certillion
			.signature()
			.toUser("talesap@gmail.com")
			.batch()
			.cades()
			.message("Petição da Lava Jato")
			.document("relátorio ministro 1", getInputStream1())
			.document("relátorio ministro 2", getInputStream2())
			.asynchSign()
			.waitTo(30)
			.save(getOutputStream1())
			.save(getOutputStream2());
	}
	
	@Test
	public void testAsynchAdobePdfBatchSignatureWaitForDocInfos() throws Exception {
		SignatureInfos info = Certillion
			.signature()
			.toUser("talesap@gmail.com")
			.batch()
			.cades()
			.message("Petição da Lava Jato")
			.document("relátorio ministro 1", getInputStream1())
			.document("relátorio ministro 2", getInputStream2())
			.asynchSignWithNotify()
			.waitForDocInfos();
		
		log.info("Transaction id: " + info.getTransactionId());
		
		Deque<DocumentStatus> documentsStatus = info.getDocumentsStatus();
		for (DocumentStatus documentStatus : documentsStatus) {
			log.info("------------");
			log.info("\tDoc ID: " + documentStatus.getDocId());
			log.info("\tName: " + documentStatus.getName());
		}
	}
	
	
	@Test
	public void testAsynchAdobePdfBatchSignatureWaitForDocInfosTwoPhases() throws Exception {
		SignatureInfos info = Certillion
			.signature()
			.toUser("talesap@gmail.com")
			.batch()
			.adobePdf()
			.message("Petição da Lava Jato")
			.document("relátorio ministro 1", getInputStream1())
			.document("relátorio ministro 2", getInputStream2())
			.asynchSign()
			.waitForDocInfos();
		
		final long transactionId = info.getTransactionId();
		log.info("Transaction id: " + transactionId);
		
		Deque<DocumentStatus> documentsStatus = info.getDocumentsStatus();
		for (DocumentStatus documentStatus : documentsStatus) {
			log.info("------------");
			log.info("\tDoc ID: " + documentStatus.getDocId());
			log.info("\tName: " + documentStatus.getName());
		}
		
		Certillion
			.signature()
			.batch()
			.adobePdf()
			.waitFor(transactionId)
			.save(getInputStream1(), getOutputStream1())
			.save(getInputStream1(), getOutputStream2());
		
	}
	
	private FileOutputStream getOutputStream2() throws FileNotFoundException {
		return new FileOutputStream("C:\\Users\\Tales Porto\\Downloads\\POLÍTICA INTERNA PDF [SIGNED].pdf");
	}


	private FileOutputStream getOutputStream1() throws FileNotFoundException {
		return new FileOutputStream("C:\\Users\\Tales Porto\\Downloads\\contrato [SIGNED].pdf");
	}


	private FileInputStream getInputStream2() throws FileNotFoundException {
		return new FileInputStream("C:\\Users\\Tales Porto\\Downloads\\POLÍTICA INTERNA PDF.pdf");
	}


	private FileInputStream getInputStream1() throws FileNotFoundException {
		return new FileInputStream("C:\\Users\\Tales Porto\\Downloads\\contrato.pdf");
	}
}
