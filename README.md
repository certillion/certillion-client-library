certillion-client-library
===============

Projeto de biblioteca cliente para consumir os serviços do Certillion


### Configuração

``` java
Certillion
	.config()
	.keystorePath("certillion_final.jks")
	.keystoreType("JKS")
	.keyAlias("*.certillion.com")
	.keystorePassword("")
	.done();
```

### Assinatura simples sincrona

``` java
Certillion
	.signature()
	.toUser("talesap@gmail.com")
	.simple()
	.message("Petição da Lava Jato")
	.sign()
	.save(getOutputStream1());
```

### Assinatura 'batch' sincrona

``` java
Certillion
	.signature()
	.toUser("talesap@gmail.com")
	.batch()
	.cades()
	.message("Petição da Lava Jato")
	.document("relatorio-ministro-1.pdf", getInputStream1())
	.document("relatorio-ministro-2.pdf", getInputStream2())
	.sign()
	.save(getOutputStream1())
	.save(getOutputStream2());
```

### Assinatura 'batch' assincrona

``` java
Certillion
	.signature()
	.toUser("talesap@gmail.com")
	.batch()
	.cades()
	.message("Petição da Lava Jato")
	.document("relatorio-ministro-1.pdf", getInputStream1())
	.document("relatorio-ministro-2.pdf", getInputStream2())
	.asynchSign()
	.waitTo(30)
	.save(getOutputStream1())
	.save(getOutputStream2());
```	

### Assinatura 'batch' assincrona com notificação

``` java
SignatureInfos info = Certillion
	.signature()
	.toUser("talesap@gmail.com")
	.batch()
	.adobePdf()
	.message("Petição da Lava Jato")
	.document("relatorio-ministro-1.pdf", getInputStream1())
	.document("relatorio-ministro-2.pdf", getInputStream2())
	.asynchSignWithNotify()
	.waitForDocInfos();
```	

``` java
Certillion
	.signature()
	.batch()
	.adobePdf()
	.waitFor(transactionId)
	.save(getInputStream1(), getOutputStream1())
	.save(getInputStream1(), getOutputStream2());
```	